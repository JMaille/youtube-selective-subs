import { Injectable } from '@angular/core';
import { User, Subscription } from '../types/user.model';
import app from 'firebase';
import 'firebase/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import {
  AngularFirestore,
  AngularFirestoreDocument,
} from '@angular/fire/firestore';
import { Observable, of, Subject } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

/**
 * Authentication service
 *
 * Uses firebase authentication with google accounts.
 * Handles all read and write operations on user document.
 */
@Injectable({ providedIn: 'root' })
export class AuthService {
  user$: Observable<User>;
  private userRef: AngularFirestoreDocument<User>;
  subscriptions$: Subscription[];
  private authState = new Subject<any>();
  private subscriptionsState = new Subject<any>();
  apiKey$: string;

  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private snackBar: MatSnackBar
  ) {
    // Get the auth state, then fetch the Firestore user document if logged in
    this.afAuth.onAuthStateChanged(async (user) => {
      if (user) {
        // Logged in
        console.log(`logged in : ${user.displayName}`);
        await this.updateUserData(user);
        this.user$ = this.afs.doc<User>(`users/${user.uid}`).valueChanges();
        this.sendAuthState(true);
      } else {
        // Logged out
        console.log('logged out');
        this.sendAuthState(false);
        this.user$ = of(null);
      }
    });
  }

  private sendAuthState(isAuthenticated: boolean) {
    this.authState.next(isAuthenticated);
  }

  getAuthState(): Observable<any> {
    return this.authState.asObservable();
  }

  private sendSubscriptionsState() {
    this.subscriptionsState.next();
  }

  getSubscriptionsState(): Observable<any> {
    return this.subscriptionsState.asObservable();
  }

  // Forces an update of subscriptionsState, prompting components observing it to refresh
  public forceSubscriptionStateUpdate() {
    this.sendSubscriptionsState();
  }

  googleSignIn() {
    const provider = new app.auth.GoogleAuthProvider();
    this.afAuth.signInWithPopup(provider);
  }

  private updateUserData({ uid, email, displayName }: User) {
    // Sets user data to firestore on login
    this.userRef = this.afs.doc(`users/${uid}`);
    this.userRef.valueChanges().subscribe((user) => {
      const oldSubs = this.subscriptions$ ? [...this.subscriptions$] : [];
      this.subscriptions$ = user.subscriptions ? user.subscriptions : [];
      if (JSON.stringify(oldSubs) != JSON.stringify(this.subscriptions$)) {
        this.sendSubscriptionsState();
      }
      if (user.apiKey) this.apiKey$ = user.apiKey;
    });

    const data = {
      uid,
      email,
      displayName,
    };

    return this.userRef.set(data, { merge: true });
  }

  async signOut() {
    await this.afAuth.signOut();
  }

  setApiKey(apiKey: string) {
    this.userRef.update({ apiKey });
  }

  async addSubscription(newSub: Subscription) {
    // If the subscription already exists, do not add it again
    if (this.subscriptions$.find((sub) => sub.id == newSub.id)) {
      this.snackBar.open('You are already subscribed to this channel', 'OK', {
        duration: 2000,
        panelClass: ['snackbar-red'],
      });
    } else {
      const data = {
        subscriptions: [...this.subscriptions$, newSub],
      };

      this.snackBar.open('Subscription added', 'OK', {
        duration: 2000,
        panelClass: ['snackbar-green'],
      });
      return this.userRef.update(data);
    }
  }

  async addFilters(subId: string, filtersRaw: string) {
    const subPos = this.subscriptions$.findIndex((sub) => sub.id == subId);
    if (subPos != -1) {
      if (filtersRaw) filtersRaw = filtersRaw.toLowerCase();
      const filtersList: string[] =
        filtersRaw == '' || filtersRaw == null ? [] : filtersRaw.split(';');

      // If a 'new' filter is already in the subscription, do not add it again
      this.subscriptions$[subPos].filters.forEach((f) => {
        const filterPos = filtersList.findIndex((filter) => filter == f);
        if (filterPos != -1) {
          filtersList.splice(filterPos, 1);
        }
      });

      if (filtersList.length > 0) {
        let updatedSubs = [...this.subscriptions$];
        updatedSubs[subPos].filters.push(...filtersList);

        const data = {
          subscriptions: updatedSubs,
        };

        this.snackBar.open('Filter(s) added', 'OK', {
          duration: 2000,
          panelClass: ['snackbar-green'],
        });
        return this.userRef.update(data);
      } else {
        this.snackBar.open('Please enter new filters', 'OK', {
          duration: 2000,
          panelClass: ['snackbar-red'],
        });
      }
    } else {
      console.error(`Subscription for channel of id ${subId} does not exist.`);
    }
  }

  async deleteSubscription(id: string) {
    const pos = this.subscriptions$.findIndex((sub) => sub.id == id);
    if (pos != -1) {
      let updatedSubs = [...this.subscriptions$];
      updatedSubs.splice(pos, 1);

      const data = {
        subscriptions: updatedSubs,
      };

      this.snackBar.open('Subscription deleted', 'OK', {
        duration: 2000,
        panelClass: ['snackbar-green'],
      });
      return this.userRef.update(data);
    } else {
      console.error(`Subscription for channel of id ${id} does not exist.`);
    }
  }

  async deleteFilter(subId: string, filter: string) {
    const subPos = this.subscriptions$.findIndex((sub) => sub.id == subId);
    if (subPos != -1) {
      const filterPos = this.subscriptions$[subPos].filters.findIndex(
        (f) => f == filter
      );
      if (filterPos != -1) {
        let updatedSubs = [...this.subscriptions$];
        updatedSubs[subPos].filters.splice(filterPos, 1);

        const data = {
          subscriptions: updatedSubs,
        };

        this.snackBar.open('Filter deleted', 'OK', {
          duration: 2000,
          panelClass: ['snackbar-green'],
        });
        return this.userRef.update(data);
      } else {
        console.error(
          `Filter ${filter} does not exist in subscription ${subId}.`
        );
      }
    } else {
      console.error(`Subscription for channel of id ${subId} does not exist.`);
    }
  }
}
