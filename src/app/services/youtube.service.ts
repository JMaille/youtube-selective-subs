import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { YtApiVideo } from '../types/yt-api-video.model';
import { YtApiPlaylistVideo } from '../types/yt-api-playlist-video.model';
import { YtApiChannel } from '../types/yt-api-channel.model';
import { AuthService } from './auth.service';

/**
 * Youtube API service
 *
 * Fetches data from the Youtube API v3.
 */
@Injectable({
  providedIn: 'root',
})
export class YoutubeService {
  constructor(private httpClient: HttpClient, public auth: AuthService) {}

  async getVideos(): Promise<YtApiVideo[]> {
    let videos: YtApiVideo[] = [];

    for (const sub of this.auth.subscriptions$) {
      const uploadsId = sub.uploadsId;
      const filters = sub.filters;
      let videoIds = '';

      const playlistVideosRequest =
        'https://youtube.googleapis.com/youtube/v3/playlistItems' +
        '?part=snippet%2CcontentDetails' +
        '&playlistId=' +
        uploadsId +
        '&maxResults=5' +
        '&key=' +
        this.auth.apiKey$;

      const playlistVideosResponse = await this.httpClient
        .get<any>(playlistVideosRequest)
        .toPromise();

      playlistVideosResponse.items.forEach(
        (item: { snippet: YtApiPlaylistVideo }) => {
          let id = item.snippet.resourceId.videoId;
          if (videoIds === '') {
            videoIds = id;
          } else {
            videoIds = videoIds.concat('%2C', id);
          }
        }
      );

      const videosRequest =
        'https://youtube.googleapis.com/youtube/v3/videos' +
        '?part=snippet%2CcontentDetails%2Cstatistics' +
        '&id=' +
        videoIds +
        '&key=' +
        this.auth.apiKey$;

      const videosResponse = await this.httpClient
        .get<any>(videosRequest)
        .toPromise();

      videosResponse.items.forEach((item: YtApiVideo) => {
        let title: string = item.snippet.title;
        // Normalize title to lowercase
        title = title.toLowerCase();
        let containsFilter = false;
        if (filters.length == 0) {
          containsFilter = true;
        } else {
          filters.forEach((filter) => {
            if (title.includes(filter)) containsFilter = true;
          });
        }
        if (containsFilter) {
          videos = [...videos, item];
        }
      });
    }

    return videos;
  }

  async getChannel(userName: string): Promise<YtApiChannel> {
    const channelRequest =
      'https://youtube.googleapis.com/youtube/v3/channels' +
      '?part=snippet%2CcontentDetails%2Cstatistics' +
      '&forUsername=' +
      userName +
      '&key=' +
      this.auth.apiKey$;

    const channelResponse = await this.httpClient
      .get<any>(channelRequest)
      .toPromise();

    if (channelResponse.pageInfo.totalResults != 0) {
      return channelResponse.items[0];
    } else {
      return null;
    }
  }
}
