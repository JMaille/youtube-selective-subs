import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainNavComponent } from './main-nav/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { VideosPanelComponent } from './videos-panel/videos-panel.component';
import { VideoCardComponent } from './video-card/video-card.component';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { SubFormComponent } from './sub-form/sub-form.component';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatCardModule } from '@angular/material/card';
import { ReactiveFormsModule } from '@angular/forms';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatGridListModule } from '@angular/material/grid-list';
import { EllipsisModule } from 'ngx-ellipsis';
import { MomentModule } from 'ngx-moment';
import { SubCardComponent } from './sub-card/sub-card.component';
import { ApiKeyFormComponent } from './api-key-form/api-key-form.component';
import { MatDialogModule } from '@angular/material/dialog';

const firebaseConfig = {
  apiKey: 'AIzaSyAPOn-aFJHreHzuegJc_73yg5GN-jYy3RM',
  authDomain: 'selectivesubs.firebaseapp.com',
  databaseURL: 'https://selectivesubs.firebaseio.com',
  projectId: 'selectivesubs',
  storageBucket: 'selectivesubs.appspot.com',
  messagingSenderId: '1086693952588',
  appId: '1:1086693952588:web:625e16a873bfa0d96978e9',
};

@NgModule({
  declarations: [
    AppComponent,
    MainNavComponent,
    VideosPanelComponent,
    VideoCardComponent,
    SubFormComponent,
    SubCardComponent,
    ApiKeyFormComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    MatInputModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,
    ReactiveFormsModule,
    MatExpansionModule,
    MatSnackBarModule,
    MatGridListModule,
    EllipsisModule,
    MomentModule,
    MatDialogModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
