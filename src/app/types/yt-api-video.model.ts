interface defaultType {
  url: string;
  width: number;
  height: number;
}

interface mediumType {
  url: string;
  width: number;
  height: number;
}

interface highType {
  url: string;
  width: number;
  height: number;
}

interface standardType {
  url: string;
  width: number;
  height: number;
}

interface maxresType {
  url: string;
  width: number;
  height: number;
}

interface thumbnailsType {
  default: defaultType;
  medium: mediumType;
  high: highType;
  standard: standardType;
  maxres: maxresType;
}

interface localizedType {
  title: string;
  description: string;
}

interface snippetType {
  publishedAt: string;
  channelId: string;
  title: string;
  description: string;
  thumbnails: thumbnailsType;
  channelTitle: string;
  tags: Array<string>;
  categoryId: string;
  liveBroadcastContent: string;
  defaultLanguage: string;
  localized: localizedType;
}

interface contentRatingType {}

interface contentDetailsType {
  duration: string;
  dimension: string;
  definition: string;
  caption: string;
  licensedContent: boolean;
  contentRating: contentRatingType;
  projection: string;
}

interface statisticsType {
  viewCount: string;
  likeCount: string;
  dislikeCount: string;
  favoriteCount: string;
  commentCount: string;
}

/**
 * Typescript version of JSON data fetched by Youtube API v3.
 * Represents a Youtube video.
 */
export interface YtApiVideo {
  kind: string;
  etag: string;
  id: string;
  snippet: snippetType;
  contentDetails: contentDetailsType;
  statistics: statisticsType;
}
