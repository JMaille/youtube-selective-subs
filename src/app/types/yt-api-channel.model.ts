interface defaultType {
  url: string;
  width: number;
  height: number;
}

interface mediumType {
  url: string;
  width: number;
  height: number;
}

interface highType {
  url: string;
  width: number;
  height: number;
}

interface thumbnailsType {
  default: defaultType;
  medium: mediumType;
  high: highType;
}

interface localizedType {
  title: string;
  description: string;
}

interface snippetType {
  title: string;
  description: string;
  customUrl: string;
  publishedAt: string;
  thumbnails: thumbnailsType;
  localized: localizedType;
  country: string;
}

interface relatedPlaylistsType {
  likes: string;
  favorites: string;
  uploads: string;
}

interface contentDetailsType {
  relatedPlaylists: relatedPlaylistsType;
}

interface statisticsType {
  viewCount: string;
  subscriberCount: string;
  hiddenSubscriberCount: boolean;
  videoCount: string;
}

/**
 * Typescript version of JSON data fetched by Youtube API v3.
 * Represents a Youtube channel.
 */
export interface YtApiChannel {
  kind: string;
  etag: string;
  id: string;
  snippet: snippetType;
  contentDetails: contentDetailsType;
  statistics: statisticsType;
}
