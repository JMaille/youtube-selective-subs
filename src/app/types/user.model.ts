/**
 * Subscription type
 *
 * Represents data stored in firestore document
 */
export interface Subscription {
  id: string;
  name: string;
  thumbnail: string;
  uploadsId: string;
  filters: string[];
}

/**
 * User type
 *
 * Represents data stored in firestore document
 */
export interface User {
  uid: string;
  email: string;
  displayName?: string;
  subscriptions?: Subscription[];
  apiKey?: string;
}
