import { Component, Input } from '@angular/core';
import { YtApiVideo } from '../types/yt-api-video.model';

/**
 * Video card component
 *
 * Displays a video thumbnail, title, creator, and time since release.
 * Clicking on the card opens the video in a new tab.
 */
@Component({
  selector: 'app-video-card',
  templateUrl: './video-card.component.html',
  styleUrls: ['./video-card.component.scss'],
})
export class VideoCardComponent {
  constructor() {}

  @Input() data: YtApiVideo;
  static iso8601DurationRegex = /(-)?P(?:([.,\d]+)Y)?(?:([.,\d]+)M)?(?:([.,\d]+)W)?(?:([.,\d]+)D)?T(?:([.,\d]+)H)?(?:([.,\d]+)M)?(?:([.,\d]+)S)?/;

  setThumbnailStyle(thumbnail: string) {
    return {
      backgroundImage: `url(${thumbnail})`,
      width: '250px',
      height: '140px',
      backgroundPosition: 'center center',
      backgroundRepeat: 'no-repeat',
      backgroundSize: 'cover',
    };
  }

  setDuration(duration: string) {
    let matches = duration.match(VideoCardComponent.iso8601DurationRegex);

    const d = {
      hours: matches[6] === undefined ? 0 : matches[6],
      minutes: matches[7] === undefined ? 0 : matches[7],
      seconds: matches[8] === undefined ? 0 : matches[8],
    };

    let readableDuration = '';
    readableDuration += d.hours == 0 ? '' : d.hours + ':';
    readableDuration += d.minutes == 0 ? '' : d.minutes + ':';
    readableDuration += d.seconds == 0 ? '' : d.seconds;

    return readableDuration;
  }

  openVideo(id: string) {
    const url = `https://www.youtube.com/watch?v=${id}`;
    window.open(url, '_blank');
  }
}
