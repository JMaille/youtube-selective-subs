import { Component, QueryList, ViewChildren } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';
import { FiltersFormService } from '../services/filters-form.service';
import { SubCardComponent } from '../sub-card/sub-card.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { ApiKeyFormComponent } from '../api-key-form/api-key-form.component';

/**
 * Main navigation component.
 *
 * Handles the display of all other components.
 */
@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.scss'],
})
export class MainNavComponent {
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(
      map((result) => result.matches),
      shareReplay()
    );
  openFormIndex: number = null;

  // List of SubCards
  @ViewChildren(SubCardComponent)
  subCards: QueryList<SubCardComponent>;

  constructor(
    private breakpointObserver: BreakpointObserver,
    public auth: AuthService,
    public filtersForm: FiltersFormService,
    public dialog: MatDialog
  ) {
    // If another filters form is open, close it
    filtersForm.openedFormIndex$.subscribe((index) => {
      if (this.openFormIndex != null) {
        this.subCards
          .find((e) => e.index == this.openFormIndex)
          .hideFiltersForm();
      }
      this.openFormIndex = index;
    });
  }

  onRefreshVideosClicked() {
    this.auth.forceSubscriptionStateUpdate();
  }

  showApiKeyDialog() {
    this.dialog.open(ApiKeyFormComponent);
  }
}