import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-api-key-form',
  templateUrl: './api-key-form.component.html',
  styleUrls: ['./api-key-form.component.scss'],
})
export class ApiKeyFormComponent {
  apiKeyForm = this.fb.group({
    apiKey: [null, Validators.required],
  });

  constructor(
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    public auth: AuthService,
    public dialog: MatDialog
  ) {}

  onSubmitApiKey() {
    if (this.apiKeyForm.invalid) {
      this.snackBar.open('Please enter an API key', 'OK', {
        duration: 2000,
        panelClass: ['snackbar-red'],
      });
    } else {
      const key: string = this.apiKeyForm.get('apiKey').value;
      this.auth.setApiKey(key);
      this.snackBar.open('API key added', 'OK', {
        duration: 2000,
        panelClass: ['snackbar-green'],
      });
    }
  }
}
