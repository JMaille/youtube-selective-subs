import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from '../services/auth.service';
import { YoutubeService } from '../services/youtube.service';
import { Subscription } from '../types/user.model';

/**
 * New subscription form component
 * 
 * Creates a subscription to the inputted channel with selected filters.
 */
@Component({
  selector: 'app-sub-form',
  templateUrl: './sub-form.component.html',
  styleUrls: ['./sub-form.component.scss'],
})
export class SubFormComponent {
  newSubForm = this.fb.group({
    username: [null, Validators.required],
    filters: null,
  });

  constructor(
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    public auth: AuthService,
    private youtubeService: YoutubeService
  ) {}

  async onSubmit() {
    if (this.newSubForm.invalid) {
      this.snackBar.open('Please enter a username', 'OK', {
        duration: 2000,
        panelClass: ['snackbar-red'],
      });
    } else {
      const userName: string = this.newSubForm.get('username').value;
      let filtersRaw: string = this.newSubForm.get('filters').value;
      if (filtersRaw) filtersRaw = filtersRaw.toLowerCase();
      const filtersList: string[] =
        filtersRaw == '' || filtersRaw == null ? [] : filtersRaw.split(';');

      // Get the corresponding channel
      const ytApiChannel = await this.youtubeService.getChannel(userName);

      if (ytApiChannel != null) {
        // Extract the useful data from it
        const newSub: Subscription = {
          id: ytApiChannel.id,
          name: ytApiChannel.snippet.title,
          thumbnail: ytApiChannel.snippet.thumbnails.default.url,
          uploadsId: ytApiChannel.contentDetails.relatedPlaylists.uploads,
          filters: filtersList,
        };

        this.newSubForm.reset();
        this.auth.addSubscription(newSub);
      } else {
        this.snackBar.open('Channel not found, please enter a valid username', 'OK', {
          duration: 5000,
          panelClass: ['snackbar-red'],
        });
      }     
    }
  }
}
